<!DOCTYPE HTML>
<html>
<head>
 <title>Foot</title>
 <meta charset="iso-8859-1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
 <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="header">
    <h1>App Foot</h1>
  </div>
  <?php
      include 'handle.php';
  if($handle) {
   echo "<u>" . ("Connexion OK") . "</u>";
   echo "<br>";
  }
  else {
   echo ("Connexion HS");
   echo mysqli_connect_error();
  }
  ?>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
  <!-- EQUIPES -->
  <div class="equipes">
    <h2>EQUIPES</h2>
<?php
$handle = mysqli_connect("localhost","root","","foot");
$query = "SELECT * FROM equipes";
$result = mysqli_query($handle,$query);
if($result) {
 echo "<br>";
 echo "Result OK";
}
else {
 echo mysqli_error($handle);
}
 echo "<ul>";
while ($line = mysqli_fetch_array($result)) {
 echo "<li>"."[".$line["id"]."]"." ".$line["pays"]." | ".$line["surnom"];
 echo "&nbsp;<a href=eq_suppr.php?id=" . $line["id"] . ">Supprimer</a>" ;
 echo "&nbsp;<a href=eq_mod.php?id=" . $line["id"] . ">Modifier</a>" . "</li>";
}
 echo "</ul>"
 ?>
 <hr>
 <div class="form">
  <form action="eq_ajout.php" method="get"><br>
    <label for="id"><u>Ajouter une equipe</u> :</label><br>
    <label for="pays">Nom de l'equipe:</label>
    <input name="pays" type="text"><br>
    <label for="surnom">Le surnom :</label>
    <input name="surnom" type="text"><br>
    <br><input type="submit">
</form>
</div>
</div>
</div
<!-- JOUEURS -->
<div class="joueurs">
  <div class="col-md-6">
  <h2>JOUEURS</h2>
<?php
$handle = mysqli_connect("localhost","root","","foot");
$query = "SELECT * FROM joueurs";
$result = mysqli_query($handle,$query);
 echo "<ul>";
while ($line = mysqli_fetch_array($result)) {
 echo "<li>"."[".$line["id"]."]"." ".$line["nom"]." | ".$line["prenom"];
 echo "&nbsp;<a href=jr_suppr.php?id=" . $line["id"] . ">Supprimer</a>" ;
 echo "&nbsp;<a href=jr_mod.php?id=" . $line["id"] . ">Modifier</a>" . "</li>";
}
 echo "</ul>"
?>
<hr>
<div class="form">
  <form action="jr_ajout.php" method="get"><br>
    <label for="id"><u>Ajouter un joueur</u> :</label><br>
    <label for="nom">Nom du joueur:</label>
    <input name="nom" type="text"><br>
    <label for="prenom">Le prenom :</label>
    <input name="prenom" type="text"><br>
    <label for="date_naiss">Date de naissance :</label>
    <input name="date_naiss" type="text"><br>
    <label for="id_equipe">ID de l'equipe :</label>
    <input name="id_equipe" type="text"><br>
    <br><input type="submit">
</form>
</div>
</div>
</div>
</div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>
